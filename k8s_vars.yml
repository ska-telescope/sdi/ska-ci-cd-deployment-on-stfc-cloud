---
debug: true
production: true

fixed_ip:

############################
# heat-cluster vars
############################

# This name is used for the Heat stack and as a prefix for the
# cluster node hostnames.
cluster_name: systems-k8s1
cluster_keypair: ska-techops
create_floating_ip: false

# External
external_network_id: 5283f642-8bd8-48b6-8608-fa3006ff4539

# # Private Network - SKA-TechOps-Private
# private_network_id: e99cdda3-c3f7-49c6-b168-7fee803c1a77
# # subnet 192.168.99.0/24 - SKA-Techops-Subnet
# Private Network - SKA-TechOps-Kubernetes1
private_network_id: 4902e0d4-a62f-4328-a9d2-b26fd0277ada
# subnet 10.200.0.0/16 - SKA-TechOps-Kubernetes-Net1

log_docker: true
log_kubernetes: true

# default is 1500 but on stfc they use 1450 and it breaks large packets eg: rsync
docker_mtu: "1450"

cluster_az: ceph

# cluster_net:
#   - { net: "SKA-TechOps-Private", subnet: "SKA-Techops-Subnet", "floatingip_net_id": "5283f642-8bd8-48b6-8608-fa3006ff4539" }

cluster_net:
  - { net: "SKA-TechOps-Kubernetes1", subnet: "SKA-TechOps-Kubernetes-Net1", "floatingip_net_id": "5283f642-8bd8-48b6-8608-fa3006ff4539" }


# mount point and volume size for generic data volume
data_filesystem: /var/lib/containerd
data_vol_size: 50
# yes - create and attach data volume
data_vol_snapshot: ""
create_data_vol: false
mount_data_vol: false

# data2_vol (/dev/vdd) size - GB
data2_vol_name: var_vol
data2_vol_size: 50
# data2_vol mount point
data2_filesystem: /var2
# activate data2 vol
# create_data2_vol is passed to the heat template conditional for creating and
# attaching the data2 volume
create_data2_vol: false
# determine whether the data2 volume should be mounted
mount_data2_vol: false

# Docker volume - we don't want it
docker_vol_size: 0

engage_nexus_mirror: http://192.168.99.204:9082
nexus_docker_hosted_port: 

docker_deb_version: 5:20.10.18~3-0~ubuntu-focal
containerd_version: 1.6.8-1
containerd_ubuntu_version: focal
ubuntu_version: focal

genericnode_loadbalancer:
  name: "{{cluster_name }}-loadbalancer"
  flavor: "c3.medium"
  image: "ubuntu-bionic-18.04-nogui"
  num_nodes: 1

genericnode_master:
  name: "master"
  flavor: "c3.large"   # 8/80/16
  image: "ubuntu-bionic-18.04-nogui"
  num_nodes: 3

genericnode_worker:
  name: "worker"
  flavor: "le2.small"
  image: "ubuntu-bionic-18.04-nogui"
  num_nodes: 5

cluster_groups:
  - "{{ genericnode_loadbalancer }}"
  - "{{ genericnode_master }}"
  - "{{ genericnode_worker }}"

# Node group assignments for cluster roles.
# These group assignments are appended to the cluster inventory file.
# The names of these roles are cross-referenced to groups referred to
# in playbooks in the ansible/ directory.
cluster_roles:
  - name: "nodes"
    groups: "{{ cluster_groups }}"
  - name: "loadbalancers"
    groups: [ "{{ genericnode_loadbalancer }}" ]
  - name: "masters"
    groups: [ "{{ genericnode_master }}" ]
  - name: "workers"
    groups: [ "{{ genericnode_worker }}" ]
  - name: "logging"
    groups: "{{ cluster_groups }}"

cluster_group_vars: # cluster wide defaults/overides
  cluster:
    ansible_user: ubuntu
    ansible_python_interpreter: python3

cluster_inventory_append: |
  [masters:vars]
  log_docker=true
  log_kubernetes=true

  [workers:vars]
  log_docker=true
  log_kubernetes=true


############################
# k8s vars
############################


# create with kubeadm alpha certs certificate-key
k8s_certificate_key: 3b42825956735f6bcaf3c8e784c0e3de61cf13cbf4e78648950a66a0cea734fd

oidc_issuer_url: 'https://gitlab.com'
oidc_client_id: '417ea12283741e0d74b22778d2dd3f5d0dcee78828c6e9a8fd5e8589025b8d2f'
oidc_username_claim: 'sub'

k8s_dns_entry: k8s.skao.stfc
k8s_external_dns_entry: k8s.stfc.skao.int
k8s_version: '1.22.5'
kubernetes_version: '{{ k8s_version }}'
helm_version: v3.7.2

# log_docker: true
# log_kubernetes: true

k8s_interface: ens.*,eth.*
pod_network_cidr: 10.10.0.0/16

calico_version: v3.18
k8s_dns_servers:
  - 192.168.99.194
use_nginx: true
use_traefik: false
kube_state_metrics: true

# can only do one cluster - v1
k8s_api_server_addr: "10.200.0.63"
k8s_api_server_port: "6443"
k8s_api_server: "https://{{ k8s_api_server_addr }}:{{ k8s_api_server_port }}"
k8s_certificate_authority: ""
k8s_client_certificate: "/dev/null"
k8s_client_key: ""
k8s_bearer_token: "/etc/prometheus/bearer.token"
k8s_username: ""

nvidia_device_plugin_affinity:
  nodeAffinity:
    requiredDuringSchedulingIgnoredDuringExecution:
      nodeSelectorTerms:
        - matchExpressions:
            - key: kubernetes.io/hostname
              operator: In
              values:
                - systems-k8s1-v100-worker-0

# Bindehub Configuration Variables
binderhub_image:
  name: artefact.skao.int/binderhub
  tag: 0.2.0-ska-0.1.0
binderhub_oci_registry_host: "192.168.99.204:9080" # pointing to private nexus instance
binderhub_oci_registry_host_full_url: "http://{{binderhub_oci_registry_host}}"
