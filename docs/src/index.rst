.. skeleton documentation master file, created by
   sphinx-quickstart on Thu May 17 15:17:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 2
  :caption: Home
  :hidden:


.. README =============================================================

.. This project most likely has it's own README. We include it here.

.. toctree::
   :maxdepth: 2
   :caption: Readme

   ../../README

.. COMMUNITY SECTION ==================================================

..

Project ska-cicd-deploy-stfc
============================

This project defines the deployment of services on the STFC Cloud allocation in SKA-TechOps.

Operations documentation can be found at https://confluence.skatelescope.org/display/SWSI/STFC+Cloud+Deployment+and+Operations .
