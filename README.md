# README

## This repository is deprecated and archived.  Please use the following repositories instead:

* [SKA Infrastructure Machinery](https://gitlab.com/ska-telescope/sdi/ska-ser-infra-machinery)
* [SKA Ansible Collections](https://gitlab.com/ska-telescope/sdi/ska-ser-ansible-collections)

Deploy SKA Resources on STFC Cloud.

## Summary

This repo builds all resources required for SKA on STFC Cloud.
[Confluence page](https://confluence.skatelescope.org/display/SWSI/STFC+Cloud+Deployment+and+Operations).

## Settings for all Deployments

### Local Environment Setup

- Install pre-requisites
  - Ubuntu system packages: `sudo apt install libffi-dev libxslt1-dev`
- Create a virtual environment: `poetry shell`
- Activate the created virtual environment: `poetry install`
- Download the OpenStackRC File from the OpenStack dashboard and source it locally: `source system-team-openrc.sh`
- Install the required ansible modules: `make install`

### Setup the SSH access to the machines

The sections will assume you are using the `ska-techops` keypairs. On different situations, change accordingly.

- Import the `ska-techops.pub` to your personal keypairs on the OpenStack Dashboard
- On the file `Makefile` set the `CLUSTER_KEYPAIR` variable as `ska-techops`
- Ask for the private key `ska-techops.pem` key to the System team and import it to your local keys: `ssh-add -i ./ska-techops.pem`
- On the file `ssh.config` update the `IdentityFile` variable to point at `ska-techops.pem`

## K8s deployment

### Setting the relevant variables

The main variables to change on the `k8s_vars.yml` file are the following:

- `cluster_name`: when redeploying the EngageSKA cluster leave it as `syscore`, otherwise, for testing, choose a proper name
- `external_network_id`: External Network ID under the `Networks` entry on the OpenStack cluster
- `private_network_id`: Internal Network ID. You are probably creating your new network here. Extract the ID and fill it here
- `cluster_net.net`: Name of the internal network
- `cluster_net.subnet`: Name of the subnet (in case you are using one)
- `cluster_net.floatingip_net_id`: `ID of the external network
- `genericnode*.image`: Image to use as the base OS for the kubernetes nodes
- `k8s_version`: K8S version to deploy (usually the latest stable)
- `helm_version`: Helm version to deploy (usually the latest stable)
- `calico_version`: Calico version to deploy (usually the latest stable)
- `nginx_ingress_version`: NGINX Ingress version to deploy (usually the latest stable)

### Deploying the K8s Cluster

Deployment is done by simply issuing the `make k8s_build` command.

### Accessing nodes and getting the Kube Config file

In order to manage the cluster from a machine that doesn't belong to the K8S cluster, we need to download the kubeconfig file locally.

- ssh into the master node `ssh ubuntu@[masternode_floating_ip]`
- download the `/etc/kubernetes/admin.conf` to your local machine
- point the `KUBECONFIG` environment variable to the `admin.conf` file: `export KUBECONFIG=~/.kube/admin.conf`

## Bug Fixes

### CoreDNS deployment fix

Currently during deployment of the cluster, it halts when deploying CoreDNS.
As a mitigation we remove the `NoSchedule` taint for the master node with the following command:

``` console
kubectl taint nodes syscore-test-master-0 node-role.kubernetes.io/master:NoSchedule-
```

### Calico deployment fix

There is a problem with the Calico deployment so we need to deploy it manually.

Issue the command:

``` console
kubectl apply -f http://docs.projectcalico.org/v3.18/manifests.calico.yaml`

```

### Nginx deployment fix

There is a problem with the Nginx Ingress deployment so we need to deploy it manually.

First do a soft reboot to all the nodes and then issue the command:

``` console
kubectl -n ingress-nginx patch deployment/ingress-nginx-controller --type='json' -p='[{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "--ingress-class=nginx"}]'
```

### Updating the Ansible Collections

The most straightforward way to update the Ansible Collections in case a new version comes out is to simply delete the `collections` folder and re-issue a `make install`.

## K8s Adding new workers to the cluster

Instructions to add a new worker to the cluster, the workers need to be added one by one.

Deployment is done by running the following make targets:

- First, create the instance, configure it and join it to the existing cluster:

    **Verify** that the variables on the `k8s_worker_vars.yml` file are compatible with the ones on the `k8s_vars.yml` file:

    **Important Ones:**
        *`cluster_name`: when redeploying the EngageSKA cluster leave it as `syscore`, otherwise, for testing, choose a proper name
        * `external_network_id`: External Network ID under the `Networks` entry on the OpenStack cluster
        *`private_network_id`: Internal Network ID. You are probably creating your new network here. Extract the ID and fill it here
        * `cluster_net.net`: Name of the internal network
        *`cluster_net.subnet`: Name of the subnet (in case you are using one)
        * `cluster_net.floatingip_net_id`: `ID of the external network
*`genericnode*.image`: Image to use as the base OS for the kubernetes nodes
*`k8s_version`: K8S version to deploy (usually the latest stable)
*`kubectl_version`: Kubectl version to be installed in Prometheus
*`k8s_loadbalancer` and `k8s_api_server_addr`: Ip of the syscore loadbalancer
*`k8s_master`: Ip of the syscore master 0

    ``` console
    make k8s_add_worker WORKER=systems-k8s1-worker-N
    ```

    Where `WORKER` is the name of the worker node to be deployed.

    This will  create a stack on openstack to each worker deployed.

* Complete the integration of the worker nodes into the cluster, monitoring and logging.

    ``` console
    make integrate_k8s
    ```
* Finally, ensure you have unchecked the port security on the STFC openstack dashboard to avoid networking issues. 
  - On openstack dashboard there will be a new row for the worker node. 
  - Under Actions select the dropdown and choose "Edit Port Security Groups"
  - Under the next Action select "Edit Port"
  - Ensure the Port Security box is unchecked. 
  - Complete for every new node created.

## Points from last deployments

- Network errors can happen due to **security groups** and **port securities** that are defined for the newly added workers.

- You may need to drain the new workers and restart the kubelet service running on it

- Make sure the kernel is the same one as the one in the other workers, command to update it is :

    ``` console
    sudo apt-get upgrade linux-image-generic
    ```

## Prometheus Deployment

### Setting the relevant variables

The main variables to change on the `prometheus_vars.yml` file are the following:

- `cluster_name`: when redeploying the EngageSKA cluster leave it as `syscore`, otherwise, for testing, choose a proper name
- `external_network_id`: External Network ID under the `Networks` entry on the OpenStack cluster
- `private_network_id`: Internal Network ID. You are probably creating your new network here. Extract the ID and fill it here
- `cluster_net.net`: Name of the internal network
- `cluster_net.subnet`: Name of the subnet (in case you are using one)
- `cluster_net.floatingip_net_id`: `ID of the external network
- `genericnode*.image`: Image to use as the base OS for the kubernetes nodes
- `k8s_version`: K8S version to deploy (usually the latest stable)
- `kubectl_version`: Kubectl version to be installed in Prometheus
- `prometheus_scrape_configs` : Machines that will be Monitored, change them to the right IPs
- `k8s_loadbalancer` and `k8s_api_server_addr`: Ip of the syscore loadbalancer
- `k8s_master`: Ip of the syscore master 0
- `k8s_client_certificate`: Location for the k8s certificate
- `k8s_bearer_token`: Location for the k8s token
- `auth_url`: OS_AUTH_URL variable from OpenStackRC File **DO NOT PUSH THIS VARIABLE TO GITLAB**
- `username`: OS_USERNAME variable from OpenStackRC File
- `password`: OS_PASSWORD variable from OpenStackRC File **DO NOT PUSH THIS VARIABLE TO GITLAB**
- `project_id`: OS_PROJECT_ID variable from OpenStackRC File **DO NOT PUSH THIS VARIABLE TO GITLAB**
- `project_name`: OS_PROJECT_NAME variable from OpenStackRC File

### Variables to ADD to the PrivateRules.mak

`SLACK_API_URL ?= ##Hook for the #prometheus-alerts slack channel SLACK_API_URL_MVP ?= ##Hook for the #proj-mvp-channel slack channel KUBECONFIG= ## Location of the kubeconfig for the sycore cluster`

You can get the slack api keys from slack apps and the relevant channel configurations under: <https://api.slack.com/apps/>, Prometheus Alerts for `SLACK_API_URL` and Prometheus SKAMPI Alerts for `SLACK_API_URL_MVP`.

### Deploying the Prometheus

Deployment is done by simply issuing the `make monitoring` command.

This will trigger a set of targets:

- `prometheus_node`: Creates the prometheus stack and launches the instance
- `prometheus_common`: Installs needed packages on the prometheus instance
- `prometheus`: Configures and deploys the prometheus services and related ones
- `node-exporter`: Deploys the node-exporter to all instances present in the prometheus tenant
- `integrate`:
  - `update_metadata`: Runs prom_helper.py that identifies scrapers on each node and updates the compute node metadata to reflect.
  - `update_scrapers`: Updates the Prometheus configuration according the metadata generated in the `update_metadata` target

### Notes About the Deployment

- The .json files in this [folder](prometheus/roles/prometheus/files/) are updated in the prometheus instance after running `make integrate`

- The `make integrate` target needs to be run each time a change is made to the OpenStack inventory ie: a node is add/deleted/networking modified/scrapers changed.

- Ports to deployed services:
  - `Prometheus`: Port 9090
  - `AlertManager`: Port 9093
  - `Gitlab-ci-pipelines-exporter`: Port 8080

### Deploying Rook

Edit rook/PrivateRules.mak to include configuration:

``` console
$ more rook/PrivateRules.mak
EXTRA_VARS=rook_etc_ceph_dir=/home/piers/git/public/ska-telescope/sdi/ska-cicd-deploy-stfc/etc/ceph rook_image=ceph/ceph:v15.2.6
KUBECONFIG=/home/piers/git/public/ska-telescope/sdi/ska-cicd-deploy-stfc/stfc_k8s_config
IMAGE=ceph/ceph:v15.2.6
ETC_CEPH=/home/piers/git/public/ska-telescope/sdi/ska-cicd-deploy-stfc/etc/ceph
```

rook_etc_ceph_dir and ETC_CEPH must be set to a directory containing the Ceph /etc/ceph configuration.
/etc/ceph configuration should be copied from ceph nodes.

KUBECONFIG must be set to the Kubernetes cluster

### Deploying Elasticstack

Edit `elasticstack_vars.yaml` so that no outdated information is present.

To deploy elasticstack afterwards, run the `make elasticstack_build` make target.
To deploy the filebeat instances that send the logs to elasticstack, run the `make logging` make target.

Note that the filebeat deployment looks at the `logging_elasticsearch_addresses` variable in the `elasticstack_vars.yaml` file to configure the filebeat instances. So if new elasticstack instances are created, ensure that the IP addresses are updated beforehand.

### Enabling Nvidia Support for New Node

To enable nvidia support of a new node with GPU devices, the variable `NVIDIA` should be set to  `true`, alongside the name of the `WORKER` and the `FLAVOR` to use for the instance server. For example:

``` console
make k8s_add_gpu_worker WORKER=systems-k8s1-v100-worker-0 FLAVOR=g-v100.x2 NVIDIA=true
```

Ensure that the flavor used attaches GPU devices to the instance server being created and that the new node name is added to the nvidia-device-plugin affinity (defined in `k8s_vars.yaml`). For example:

``` console
nvidia_device_plugin_affinity:
  nodeAffinity:
    requiredDuringSchedulingIgnoredDuringExecution:
      nodeSelectorTerms:
        - matchExpressions:
            - key: kubernetes.io/hostname
              operator: In
              values:
                - systems-k8s1-v100-worker-0
```

## Deploy binderhub

Variables declared as default in the binderhub role in ansible-roles can be overriden in k8s_vars.yaml In the **PrivateRules.mak** add and fill the following vars
for the Azure authentication:

``` console
AZUREAD_CLIENT_ID=<AZUREAD_CLIENT_ID> # Optional, has default value
AZUREAD_CLIENT_SECRET=<AZUREAD_CLIENT_SECRET>
AZUREAD_TENANT_ID=<AZUREAD_TENANT_ID> # Optional, has default value
BINDERHUB_OCI_REGISTRY_PASSWORD=<BINDERHUB_OCI_REGISTRY_PASSWORD> # Password for user <binderhub_oci_registry_username> at <binderhub_oci_registry_host>
```

Then you need to download the ansible collections

``` console
make install
```

Now you can install binderhub:

``` console
make deploy_binderhub
```

## Signing and distributing certificates

A certificate authority is created and signed by the *Terminus* host enabling certificates to
be distributed to the requested clients.

### Setup

A `PrivateRules.mak` must exist on the root folder of this repository with the `CA_CERT_PASS`
key set to the password for the certificate authority.

### Setup the Certificate Authority (CA) on Terminus

The CA resides on the Terminus host. In case you are creating from scratch, you should use the following commands:

``` console
make bifrost ca_setup
```

### Deploying the certificates on the clients

The certificates are signed and distributed to the clients by setting up the relevant hosts on `inventory_elasticstack`
under the group `[ca]` and then issuing the command:

``` console
make bifrost ca_sign
```

### Revoking certificates

In case we want to revoke a certificate, we can fill in the `[ca_revoke]` group under the `inventory_elasticstack` hosts file and issue the following command:

``` console
make bifrost ca_revoke
```

### Deleting the certificates on the clients (WARNING: this will delete all certificates!)

If you need to delete all certificates present on the clients, you can issue the following command:

``` console
make bifrost ca_delete
```

This will delete the certificates on all the clients under the `[ca]` group on the `inventory_elasticstack` hosts file.
