ANSIBLE_USER ?= ubuntu## ansible user for the playbooks
CLUSTER_KEYPAIR ?= ska-techops## key pair available on openstack to be put in the created VMs
DEBUG ?= false
CONTAINERD ?= true
NVIDIA ?= false
IGNORE_NVIDIA_FAIL ?= false
TAGS ?=
COLLECTIONS_PATHS ?= ./collections
PODMAN_REGISTRY_MIRROR ?= dockerhub.stfc.ac.uk
REGISTRY_MIRROR ?= https://$(PODMAN_REGISTRY_MIRROR)
V ?=
THIS_BASE=$(shell pwd)

# used for determining redirection to git submodules
TARGET_CMD=$(shell echo $(MAKECMDGOALS) | cut -d ' ' -f1)
TARGET_ARGS=$(shell echo $(MAKECMDGOALS) | cut -d ' ' -f2-)

OPENSTACK_ENV_VARIABLES=$(shell env | grep OS_ )

.DEFAULT_GOAL := help

.IGNORE: build elasticstack_build build_logging ceph build_stack

.PHONY: bifrost dns openvpn prometheus elasticstack elasticstack_build k8s ceph rook nexus mariadb influxdb help build_elasticstack_haproxy

## Bifrost args
BIFROST_VARS ?= bifrost_vars.yml
BIFROST_CLUSTER_INVENTORY ?= ../inventory_bifrost
# BIFROST_RUN_TAGS ?= ansible,jumphost,openstack,k8s,ceph
BIFROST_RUN_TAGS ?= ansible,jumphost,dns,openstack,k8s,
BIFROST_CLUSTER_NAME ?= terminus
BIFROST_EXTRA_VARS ?= registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' ca_cert_pass='$(CA_CERT_PASS)' client_cert_prefix='$(CLIENT_CERT_PREFIX)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)' jump_host=' -F $(THIS_BASE)/ssh.config $(BIFROST_CLUSTER_NAME) '
KUBECONFIG_FILE ?="/etc/stfc_k8s_config"
RESCUE_PASSWORD ?= ""
MARVIN_SLACK_TOKEN ?=
GITLAB_TOKEN ?= ******

## Prometheus args
PROMETHEUS_VARS ?= prometheus_vars.yml
PROMETHEUS_CLUSTER_INVENTORY ?= inventory_prometheus
SLACK_API_URL ?= ******************
SLACK_API_URL_MVP ?= ******************
SLACK_CHANNEL ?= prometheus-alerts
SLACK_CHANNEL_MVP ?= proj-mvp-messages
PROMETHEUS_ALERTMANAGER_URL ?= http://monitoring.skao.stfc:9093
PROMETHEUS_URL ?= http://monitoring.skao.stfc:9095
PROM_CONFIGS_PATH ?= .
MONITORING_NODES ?= cluster
ARCHIVER_PASSWORD ?= "mandatory"
GRAFANA_PASSWORD ?= "mandatory"
TANGODB_PASSWORD ?= "mandatory"
KUBECONFIG ?= "/etc/kubernetes/admin.conf"

## ElsticStack args
ELASTICSTACK_VARS ?= elasticstack_vars.yml
LOGGING_NODES ?= all
LOGGING_INVENTORY_FILE ?= combined_inventory

## Ceph args
CEPH_VARS ?= ceph_vars.yml
CEPH_CLUSTER_INVENTORY ?= inventory_ceph
CEPH_EXTRA_VARS ?=

## K8s args
K8S_VARS ?= k8s_vars.yml
K8S_CLUSTER_INVENTORY ?= inventory_k8s
K8S_EXTRA_VARS ?=
K8S_RUN_TAGS ?=
K8S_NODES ?=

## VPN args
OPENVPN_CLIENT ?=
OPENVPN_CLIENT_EMAIL ?=
KEYSERVER ?= keyserver.ubuntu.com

## Nexus args
NEXUS_VARS ?= nexus_vars.yml
NEXUS_CLUSTER_INVENTORY ?= inventory_nexus

## MariaDB args
MARIADB_VARS ?= mariadb_vars.yml
MARIADB_CLUSTER_INVENTORY ?= inventory_mariadb
MYSQL_ROOT_PASSWORD?=secret## MySQL root user password
MYSQL_DATABASE?=tango## Database created by default when playbook is run
MYSQL_USER?=eda_admin## Username of MySQL Admin user to be distributed to the team using the database
MYSQL_PASSWORD?=another_secret## Additional Admin user's password
MYSQL_PRIVILEGES?='*.*:ALL,GRANT'## Privileges granted to the Admin user

## InfluxDB args
INFLUXDB_VARS ?= influxdb_vars.yml
INFLUXDB_CLUSTER_INVENTORY ?= inventory_influxdb

## Binderhub args
BINDERHUB_OCI_REGISTRY_PASSWORD ?=

# AzureAD vars
AZUREAD_CLIENT_ID ?= 45aef1d7-9fcc-4e52-a572-b158d2a8b856
AZUREAD_CLIENT_SECRET ?=
AZUREAD_TENANT_ID ?= 78887040-bad7-494b-8760-88dcacfb3805

# define overides for above variables in here
-include PrivateRules.mak

# calculated from private rules
PROMETHEUS_EXTRA_VARS ?= mode='server' slack_channel='$(SLACK_CHANNEL)' slack_channel_mvp='$(SLACK_CHANNEL_MVP)' slack_api_url_mvp='$(SLACK_API_URL_MVP)' prometheus_alertmanager_url='$(PROMETHEUS_ALERTMANAGER_URL)' project_name='$(OS_PROJECT_NAME)' project_id='$(OS_PROJECT_ID)' auth_url='$(OS_AUTH_URL)' username='$(OS_USERNAME)' password='$(OS_PASSWORD)' prometheus_url='$(PROMETHEUS_URL)' prometheus_gitlab_ci_pipelines_exporter_token='$(GITLAB_TOKEN)'


# collections and roles locations
ANSIBLE_COLLECTIONS_PATHS := $(COLLECTIONS_PATHS):~/.ansible/collections:/usr/share/ansible/collections
DOCKER_PLAYBOOKS := $(COLLECTIONS_PATHS)/ansible_collections/ska_cicd/docker_base/playbooks
DOCKER_ROLES := $(COLLECTIONS_PATHS)/ansible_collections/ska_cicd/docker_base/roles/docker
STACK_CLUSTER_PLAYBOOKS := $(COLLECTIONS_PATHS)/ansible_collections/ska_cicd/stack_cluster/playbooks
K8S_PLAYBOOKS := $(COLLECTIONS_PATHS)/ansible_collections/ska_cicd/k8s/playbooks


# If the first argument is "run"...
ifeq (bifrost,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "bifrost"
  TARGET_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(TARGET_ARGS):;@:)
endif

vars:  ## Variables
	@echo "Current variable settings:"
	@echo "NODE_NAME=$(NODE_NAME)"
	@echo "NODE_VARS=$(NODE_VARS)"
	@echo "CLUSTER_KEYPAIR=$(CLUSTER_KEYPAIR)"
	@echo "INVENTORY_FILE=$(INVENTORY_FILE)"
	@echo "BIFROST_EXTRA_VARS=$(BIFROST_EXTRA_VARS)"
	@echo "ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS)"
	@echo "STACK_CLUSTER_PLAYBOOKS=$(STACK_CLUSTER_PLAYBOOKS)"
	@echo "DOCKER_PLAYBOOKS=$(DOCKER_PLAYBOOKS)"
	@echo "DEBUG=$(DEBUG)"
	@echo "K8S_EXTRA_VARS=$(K8S_EXTRA_VARS)"
	@echo "AZUREAD_CLIENT_ID=$(AZUREAD_CLIENT_ID)"
	@echo "AZUREAD_TENANT_ID=$(AZUREAD_TENANT_ID)"

sub:  ## update git submodules
	git submodule init
	git submodule update --recursive --remote
#	git submodule update --init --recursive

uninstall:  # uninstall collections
	rm -rf $(COLLECTIONS_PATHS)/ansible_collections/* $(COLLECTIONS_PATHS)/.collected

install:  ## Install dependent ansible collections
	if [ -f $(COLLECTIONS_PATHS)/.collected ]; then \
	echo "Allready collected !"; \
	else \
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-galaxy collection install \
	-r requirements.yml -p ./collections; \
	ansible-galaxy install -r requirements.yml --roles-path ./playbooks/roles; \
	touch $(COLLECTIONS_PATHS)/.collected; \
	fi

reinstall: uninstall install ## reinstall collections


lint: install ## Lint check playbooks and roles
	yamllint -d "{extends: relaxed, rules: {line-length: {max: 256}}}" \
			-f parsable \
			playbooks/roles/* \
			playbooks/*.yml \
			| yamllint-junit -o linting-yamllint.xml;
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-lint --nocolor --exclude=playbooks/roles/nvidia.nvidia_driver/ --exclude=playbooks/site.yml --exclude=playbooks/purge-cluster.yml playbooks/roles/* \playbooks/*.yml -p | tee ansible-lint.txt;
	ansible-lint-junit ansible-lint.txt -o linting-ansible.xml
	flake8 --format junit-xml playbooks/roles/* --output-file linting-flake.xml

# Join different linting reports into linting.xml
# Zero, create linting.xml with empty testsuites
# First, delete newlines from the files for easier parsing
# Second, parse <testsuite> tags in <testsuites> in each file (disregard any attributes in testsuites tag)
# Final, append <testsuite> tags into linting.xml
join-lint-reports:
	@echo "<testsuites>\n</testsuites>" > build/reports/linting.xml; \
	for FILE in linting-*.xml; do \
	TEST_RESULTS=$$(tr -d "\n" < $${FILE} | \
	sed -e "s/.*<testsuites[^<]*\(.*\)<\/testsuites>.*/\1/"); \
	TT=$$(echo $${TEST_RESULTS} | sed 's/\//\\\//g'); \
	sed -i.x -e "/<\/testsuites>/ s/.*/$${TT}\n&/" build/reports/linting.xml; \
	rm -f build/reports/linting.xml.x; \
	done
.PHONY: join-lint-reports

join-lint-reports-python-script:
	python3 join_lint_reports.py


bifrost: ## call the Makefile in the bifrost sub-project
	export ANSIBLE_SSH_ARGS="-o ForwardAgent=yes -F $(THIS_BASE)/ssh.config " && \
	cd bifrost && make $(TARGET_ARGS) \
		PRIVATE_VARS=$(THIS_BASE)/$(BIFROST_VARS) \
		CLUSTER_KEYPAIR=$(CLUSTER_KEYPAIR) \
		INVENTORY_FILE=../inventory_bifrost \
		CA_TARGET_INVENTORY=$(THIS_BASE)/inventory_elasticstack \
		CLUSTER_INVENTORY=$(BIFROST_CLUSTER_INVENTORY) \
		RUN_TAGS=$(BIFROST_RUN_TAGS) \
		EXTRA_VARS=$(BIFROST_EXTRA_VARS) \
		V=$(V) \
		MARVIN_SLACK_TOKEN=$(MARVIN_SLACK_TOKEN)

bifrost_build: ## all the bifrost node setup steps
	# make bifrost build_node ## nolonger works because Ubuntu-Bionic-NoGui has been deleted!!!!
	make bifrost build_common
	make bifrost build_docker
	make bifrost build_bifrost

cron: ## Install cron jobs on bifrost node
	ansible-playbook -i inventory_bifrost playbooks/cronjobs.yml \
					 --extra-vars="kubeconfig_file=$(KUBECONFIG_FILE)" $(V)

openvpn: ## setup OpenVPN on the bifrost node
	cd playbooks && \
	ansible-playbook  -i ../inventory_bifrost openvpn_server.yml \
					  -e @../$(BIFROST_VARS) \
					  --extra-vars="$(BIFROST_EXTRA_VARS)" $(V)

mkvpnclient: ## Create VPN Client Certificates
	ansible -i ./inventory_bifrost bifrost \
	                         -b -m shell -a 'cd /etc/openvpn/easy-rsa/ && ./mkclient.sh $(OPENVPN_CLIENT)'
	ansible -i ./inventory_bifrost bifrost \
	                         -b -m shell -a 'cd /etc/openvpn/easy-rsa/ && ./mkconfig.sh $(OPENVPN_CLIENT)'

getvpnclient: ## Get VPN Client Config
	ansible -i ./inventory_bifrost bifrost \
	                         -b -m fetch -a 'src=/etc/openvpn/easy-rsa/client-configs/$(OPENVPN_CLIENT).ovpn dest=$(THIS_BASE)/ flat=yes'

vpnclient: mkvpnclient getvpnclient ## Create OpenVPN client certs and config and fetch (user name: OPENVPN_CLIENT)

import_gpg_key: ## Import GPG key from keyserver
	gpg --keyserver $(KEYSERVER) --search-keys $(OPENVPN_CLIENT_EMAIL)

encrypt_vpnclient: import_gpg_key ## Encrypt OVPN file using gpg
	gpg --output $(THIS_BASE)/$(OPENVPN_CLIENT).ovpn.gpg --encrypt --recipient $(OPENVPN_CLIENT_EMAIL) $(THIS_BASE)/$(OPENVPN_CLIENT).ovpn

gpg_encrypted_vpnclient: ## vpnclient encrypt_vpnclient ## Create client certs, config, fetch and encrypt (user name: OPENVPN_CLIENT, email: OPENVPN_CLIENT_EMAIL)
	@pwd && ls -latr | grep $(OPENVPN_CLIENT)

rm_vpnclient: ## Revoke VPN cert and remove records
	ansible -i ./inventory_bifrost bifrost \
	                         -b -m shell -a 'cd /etc/openvpn/easy-rsa/ && ./revoke.sh $(OPENVPN_CLIENT)'

set_inventory:
	mkdir -p combined_inventory
	rm -f combined_inventory/*
	cp inventory_* combined_inventory/
	rm -f combined_inventory/*.save combined_inventory/*.backup
	cat combined_inventory/inventory_* > all_inventory

dns: set_inventory ## setup simple DNS server on bifrost node
	cd playbooks && \
	ansible-playbook  -i ../combined_inventory dns_server.yml \
					  -e @../$(BIFROST_VARS) \
					  --extra-vars=$(BIFROST_EXTRA_VARS) $(V)

ceph_tools: set_inventory ## Install Ceph client tools on bifrost node
	cd playbooks && \
	ansible-playbook  -i ../combined_inventory ceph_tools.yml \
					  -e @../$(BIFROST_VARS) \
					  --extra-vars="$(BIFROST_EXTRA_VARS)" $(V)

reverseproxy: set_inventory ## Install Ceph client tools on bifrost node
	cd playbooks && \
	ansible-playbook  -i ../combined_inventory proxy.yml \
					  -e @../$(BIFROST_VARS) \
					  --extra-vars="$(BIFROST_EXTRA_VARS) oauth2proxy_client_id='$(CLIENT_ID)' oauth2proxy_client_secret='$(CLIENT_SECRET)' oauth2proxy_cookie_secret='$(COOKIE_SECRET)'" oauth2proxy_tenant_id='$(TENANT_ID)'" $(V)

update_admins: set_inventory ## Update admin access
	cd playbooks && \
	ansible-playbook  -i ../combined_inventory --limit cluster update_admins.yml \
					  -e @../$(BIFROST_VARS) \
					  --extra-vars="$(BIFROST_EXTRA_VARS)" $(V)

management: bifrost_build openvpn dns ceph_tools reverseproxy cron update_admins  ## setup the management (bifrost) node

management_extensions: openvpn dns ceph_tools reverseproxy cron update_admins  ## setup the management (bifrost) node

rescue_account: set_inventory ## Update admin access
	cd playbooks && \
	ansible-playbook  -i ../combined_inventory rescue_account.yml \
					  -e @../$(BIFROST_VARS) \
					  --extra-vars="$(BIFROST_EXTRA_VARS)" \
					  --extra-vars="rescue_password=$(RESCUE_PASSWORD)" $(V)

prometheus_node: ## Prometheus Build nodes based on heat-cluster
	[ ! -e $(PROMETHEUS_VARS) ] || touch $(PROMETHEUS_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -i $(STACK_CLUSTER_PLAYBOOKS)/inventory \
					 $(STACK_CLUSTER_PLAYBOOKS)/cluster-infra.yml \
					 -e @$(THIS_BASE)/$(PROMETHEUS_VARS) \
					 --extra-vars="$(PROMETHEUS_EXTRA_VARS)" \
					 --extra-vars="cluster_keypair='$(CLUSTER_KEYPAIR)'" \
					 --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
					 --extra-vars="jump_host=' -F $(THIS_BASE)/ssh.config $(BIFROST_CLUSTER_NAME) '" \
					 --extra-vars="inventory_file=../../../../$(PROMETHEUS_CLUSTER_INVENTORY)" $(V)

prometheus_common:  ## Prometheus apply the common roles
	[ ! -e $(PROMETHEUS_VARS) ] || touch $(PROMETHEUS_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(PROMETHEUS_CLUSTER_INVENTORY) $(STACK_CLUSTER_PLAYBOOKS)/common.yml \
					  -e @$(THIS_BASE)/$(PROMETHEUS_VARS) \
					  --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
					  --extra-vars="$(PROMETHEUS_EXTRA_VARS)" $(V)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(PROMETHEUS_CLUSTER_INVENTORY) $(DOCKER_PLAYBOOKS)/docker.yml \
					  -e @$(THIS_BASE)/$(PROMETHEUS_VARS) \
					  --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
					  --extra-vars="$(PROMETHEUS_EXTRA_VARS)" $(V)

prometheus: ## call the Makefile in the prometheus sub-project
	[ -f prometheus/prometheus_node_metric_relabel_configs.yaml ] || echo 'dummy:' > prometheus/prometheus_node_metric_relabel_configs.yaml
	export ANSIBLE_SSH_ARGS="-o ForwardAgent=yes -F $(THIS_BASE)/ssh.config " && \
	cd prometheus && \
	ansible-playbook deploy_prometheus.yml -b \
		-e @$(THIS_BASE)/$(PROMETHEUS_VARS) \
		--extra-vars "mode='server' slack_api_url='$(SLACK_API_URL)' slack_api_url_mvp='$(SLACK_API_URL_MVP)'" \
		--extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
		--extra-vars="azuread_client_id='$(AZUREAD_CLIENT_ID)' azuread_client_secret='$(AZUREAD_CLIENT_SECRET)' azuread_tenant_id='$(AZUREAD_TENANT_ID)'" \
		-e "slack_channel='$(SLACK_CHANNEL)'" \
		-e "slack_channel_mvp='$(SLACK_CHANNEL_MVP)'" \
		-e "prometheus_alertmanager_url='$(PROMETHEUS_ALERTMANAGER_URL)'" \
		-e "project_id='$(OS_PROJECT_ID)' project_name='$(OS_PROJECT_NAME)' auth_url='$(OS_AUTH_URL)'" \
		-e "archiver_password='$(ARCHIVER_PASSWORD)' grafana_admin_password='$(GRAFANA_PASSWORD)' tangodb_password='$(TANGODB_PASSWORD)' kubeconfig='$(KUBECONFIG)'" \
		-e "username='$(OS_USERNAME)' password='$(OS_PASSWORD)'" \
		-e "prometheus_url='$(PROMETHEUS_URL)'" \
		-e "prometheus_data_dir='/var/lib/prometheus'" \
		-i $(THIS_BASE)/inventory_prometheus \
		-e @$(PROM_CONFIGS_PATH)/prometheus_node_metric_relabel_configs.yaml \
		-e 'ansible_python_interpreter=/usr/bin/python3' \
		--skip-tags=crontab --extra-vars="$(PROMETHEUS_EXTRA_VARS)" $(V)
	make update_scrapers

prometheus_thanos_server: ## Install Thanos query and query front-end
	export ANSIBLE_SSH_ARGS="-o ForwardAgent=yes -F $(THIS_BASE)/ssh.config " && \
	cd prometheus && \
	ansible-playbook deploy_prometheus.yml -b \
		-e @$(THIS_BASE)/$(PROMETHEUS_VARS) \
		--extra-vars "mode='thanos'" \
		-i $(THIS_BASE)/inventory_prometheus \
		-e 'ansible_python_interpreter=/usr/bin/python3'

# monitoring: prometheus_node prometheus_common prometheus ## build all of Prometheus  # cannot do prometheus_node anymore because of VM image

monitoring: prometheus_common prometheus ## build all of Prometheus

node-exporter: set_inventory ## deploy node_exporter
	export ANSIBLE_SSH_ARGS="-o ForwardAgent=yes -F $(THIS_BASE)/ssh.config " && \
	cd prometheus && \
	make node-exporter INVENTORY_FILE=$(THIS_BASE)/combined_inventory NODES=$(MONITORING_NODES) EXTRA_VARS=$(THIS_BASE)/$(PROMETHEUS_VARS)

update_metadata: set_inventory
	ansible -i $(PROMETHEUS_CLUSTER_INVENTORY) cluster -b -m copy -a 'src=./all_inventory dest=/tmp/all_inventory'
	@ansible -i $(PROMETHEUS_CLUSTER_INVENTORY) cluster -b -m shell -a 'export project_id=$(OS_PROJECT_ID) project_name=$(OS_PROJECT_NAME) auth_url=$(OS_AUTH_URL) username=$(OS_USERNAME) password=$(OS_PASSWORD) $(OPENSTACK_ENV_VARIABLES) && python3 /usr/local/bin/prom_helper.py -u /tmp/all_inventory'

update_scrapers:
	@ansible -i $(PROMETHEUS_CLUSTER_INVENTORY) prometheus -b -m shell -a 'export project_id=$(OS_PROJECT_ID) project_name=$(OS_PROJECT_NAME) auth_url=$(OS_AUTH_URL) username=$(OS_USERNAME) password=$(OS_PASSWORD) $(OPENSTACK_ENV_VARIABLES) && cd /etc/prometheus && python3 /usr/local/bin/prom_helper.py -g'
	cd prometheus && scp -F ../ssh.config prometheus:/etc/prometheus/prometheus_node_metric_relabel_configs.yaml .

integrate: update_metadata update_scrapers ## Integrate all node into cluster

elasticstack: ## call the Makefile in the elasticstack sub-project
	export ANSIBLE_SSH_ARGS="-o ForwardAgent=yes -F $(THIS_BASE)/ssh.config " && \
	cd elasticstack && make $(TARGET_ARGS) \
		PRIVATE_VARS=$(THIS_BASE)/$(ELASTICSTACK_VARS) CLUSTER_KEYPAIR=$(CLUSTER_KEYPAIR) \
		INVENTORY_FILE=$(THIS_BASE)/inventory_elasticstack LOGGING_INVENTORY_FILE=$(THIS_BASE)/inventory_elasticstack CLUSTER_INVENTORY=$(ELASTICSTACK_CLUSTER_INVENTORY) \
		RUN_TAGS=$(ELASTICSTACK_RUN_TAGS) EXTRA_VARS=$(BIFROST_EXTRA_VARS)

elasticstack_build: ## all the elasticstack node setup steps
	make elasticstack install
	# make elasticstack build_nodes  # cannot do nexus_node anymore because of VM image
	make elasticstack build_stack

elasticstack_docker: ## Run the elasticstack docker build
	make elasticstack build_docker
	
build_elasticstack_haproxy: ## Build haproxy for ELK
	make elasticstack build_elk_haproxy

elasticstack_build_loadbalancer:
	export ANSIBLE_SSH_ARGS="-o ForwardAgent=yes -F $(THIS_BASE)/ssh.config " && \
	cd elasticstack && make build_nodes  \
		PRIVATE_VARS=$(THIS_BASE)/elasticstack_loadbalancer_vars.yml CLUSTER_KEYPAIR=$(CLUSTER_KEYPAIR) \
		INVENTORY_FILE=../inventory_elasticstack_loadbalancer \
		RUN_TAGS=$(ELASTICSTACK_RUN_TAGS) EXTRA_VARS=$(BIFROST_EXTRA_VARS)
	make build_elasticstack_haproxy ELASTICSTACK_VARS=elasticstack_loadbalancer_vars.yml

build_logging: set_inventory filebeat rotate ## Install all logging clients

logging: build_logging

# curl -XPUT --retry-connrefused --connect-timeout 5 --retry 999  --retry-delay 1 "http://localhost:9200"/_ingest/pipeline/ska_log_parsing_pipeline -H "kbn-xsrf: true" -H "Content-Type: application/json" -d @/etc/ska_log_parsing_pipeline.json

filebeat:  ## Install filebeat
	export ANSIBLE_SSH_ARGS="-o ForwardAgent=yes -F $(THIS_BASE)/ssh.config " && \
	cd elasticstack && ansible-playbook -i $(THIS_BASE)/$(LOGGING_INVENTORY_FILE) -l $(LOGGING_NODES) playbooks/logging.yml \
	 -e @$(THIS_BASE)/$(ELASTICSTACK_VARS) \
	 --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)' jump_host=' -F $(THIS_BASE)/ssh.config $(BIFROST_CLUSTER_NAME) ' $(EXTRA_VARS)" \
	 --tags filebeat $(V)

rotate:  ## Install rotate
	export ANSIBLE_SSH_ARGS="-o ForwardAgent=yes -F $(THIS_BASE)/ssh.config " && \
	cd elasticstack && ansible-playbook -i $(THIS_BASE)/$(LOGGING_INVENTORY_FILE) -l $(LOGGING_NODES) playbooks/logging.yml \
	 -e @$(THIS_BASE)/$(ELASTICSTACK_VARS) \
	 --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)' jump_host=' -F $(THIS_BASE)/ssh.config $(BIFROST_CLUSTER_NAME) ' $(EXTRA_VARS)" \
	 --tags rotate $(V)

stackcheck: ## ElasticStack Health check
	ip=`grep 'master-0.*ansible_host' ./inventory_elasticstack | cut -d '=' -f 2 | cut -d ' ' -f 1`; echo $$ip; \
	ansible -i ./inventory_bifrost bifrost -b -m shell -a "curl -s 'http://$${ip}:9200/_cluster/health/?wait_for_status=yellow&timeout=50s&pretty'"

stacknodecheck: ## ElasticStack Node state
	ip=`grep 'master-0.*ansible_host' ./inventory_elasticstack | cut -d '=' -f 2 | cut -d ' ' -f 1`; \
	ansible -i ./inventory_bifrost bifrost -b -m shell -a "curl -s 'http://$${ip}:9200/_nodes/process?pretty'"

stackstatecheck: ## ElasticStack Whole cluster state
	ip=`grep 'master-0.*ansible_host' ./inventory_elasticstack | cut -d '=' -f 2 | cut -d ' ' -f 1`; \
	ansible -i ./inventory_bifrost bifrost -b -m shell -a "curl -s 'http://$${ip}:9200/_cluster/state?pretty'"

ceph_nodes: ## Ceph Build nodes based on heat-cluster
	[ ! -e $(CEPH_VARS) ] || touch $(CEPH_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -i $(STACK_CLUSTER_PLAYBOOKS)/inventory \
					 $(STACK_CLUSTER_PLAYBOOKS)/cluster-infra.yml \
					 -e @$(THIS_BASE)/$(CEPH_VARS) \
					 --extra-vars="$(CEPH_EXTRA_VARS)" \
					 --extra-vars="cluster_keypair='$(CLUSTER_KEYPAIR)'" \
					 --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
					 --extra-vars="jump_host=' -F $(THIS_BASE)/ssh.config $(BIFROST_CLUSTER_NAME) '" \
					 --extra-vars="inventory_file=../../../../$(CEPH_CLUSTER_INVENTORY)" $(V)
	perl -ne 'next unless $$_ =~ /master\-/; ($$h,$$d1,$$d2) = $$_ =~ /^([^\s]+).*?data_vol_diskid\=\"([^\"]+)\"\s+data2_vol_diskid\=\"([^\"]+)\"/; print "$$h ceph_data_device1=/dev/disk/by-id/virtio-$$d1 ceph_wal_device1=/dev/disk/by-id/virtio-$$d2\n"' $(CEPH_CLUSTER_INVENTORY) > /tmp/ceph_vols.txt
	cat $(CEPH_CLUSTER_INVENTORY) >> /tmp/ceph_vols.txt
	# mons,osds,mdss,rgws,nfss,rbdmirrors,clients,mgrs,iscsigws,iscsi-gws,grafana-server,rgwloadbalancers
	mv /tmp/ceph_vols.txt $(CEPH_CLUSTER_INVENTORY)

ceph-create-lvms: ## Create Ceph LVMs
	ansible-playbook \
			-i ./$(CEPH_CLUSTER_INVENTORY) ceph/playbooks/create-disks.yml \
			-e ireallymeanit=yes

ceph_common:  ## Ceph apply the common roles
	[ ! -e $(CEPH_VARS) ] || touch $(CEPH_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(CEPH_CLUSTER_INVENTORY) $(STACK_CLUSTER_PLAYBOOKS)/common.yml \
					  -e @$(THIS_BASE)/$(CEPH_VARS) \
					  --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
					  --extra-vars="$(CEPH_EXTRA_VARS)" $(V)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(CEPH_CLUSTER_INVENTORY) $(DOCKER_PLAYBOOKS)/docker.yml \
					  -e @$(THIS_BASE)/$(CEPH_VARS) \
					  --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
					  --extra-vars="$(CEPH_EXTRA_VARS)" $(V)

ceph_vars:
	@echo "override playbooks and variables:"
	cp -f playbooks/site.yml ceph/ceph-ansible/site.yml
	cp -f playbooks/purge-cluster.yml ceph/ceph-ansible/purge-cluster.yml
	cp -f ssh.config ceph/ceph-ansible/ssh.config
	cp -f playbooks/group_vars/ubuntu/*.yml ceph/ceph-ansible/group_vars/
	cd ceph && 	cp -f playbooks/roles/ceph-container-engine/vars/Ubuntu-18.yml \
	      ceph-ansible/roles/ceph-container-engine/vars/Ubuntu-18.yml

build_ceph: ceph_vars ## Build Ceph using project ceph-ansible
	export ANSIBLE_SSH_ARGS="-o ForwardAgent=yes -F $(THIS_BASE)/ssh.config " && \
	cd ceph/ceph-ansible && ansible-playbook -i ../../$(CEPH_CLUSTER_INVENTORY) site.yml \
			-e @$(THIS_BASE)/$(CEPH_VARS) -e "ansible_ssh_common_args='-F ssh.config'"


ceph_build: ## all the ceph node setup steps
	# make ceph_nodes  # cannot do ceph_nodes anymore because of VM image
	make ceph_common
	make ceph-create-lvms
	make build_ceph

ceph-apply-patch:  ## apply patch to upstream ceph-ansible
	# patch the nexus install for the oss edition
	if [ -f ceph/ceph-ansible/.patched ]; then \
	echo "Allready patched !"; \
	else \
	cd ceph/ && git apply --directory ceph-ansible \
		--verbose ../resources/ceph-ansible.patch  && \
	touch ceph-ansible/.patched; \
	fi

ceph-update-and-patch:  ## checkout upstream and apply patch for dev
	rm -rf ceph/ceph-ansible
	cd ceph/ && git clone git@github.com:ceph/ceph-ansible.git
	cd ceph/ceph-ansible && git checkout --track origin/stable-5.0 -b stable-5.0
	make ceph-apply-patch
	cd ceph/ceph-ansible && git diff

ceph-make-patch:  ## make updated patch for upstream
	cd ceph/ceph-ansible && git diff > ../../resources/ceph-ansible.patch

k8s_nodes: ## Kubernetes Build nodes based on heat-cluster
	[ ! -e $(K8S_VARS) ] || touch $(K8S_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -i $(STACK_CLUSTER_PLAYBOOKS)/inventory \
					 $(STACK_CLUSTER_PLAYBOOKS)/cluster-infra.yml \
					 -e @$(THIS_BASE)/$(K8S_VARS) \
					 --extra-vars="$(K8S_EXTRA_VARS)" \
					 --extra-vars="cluster_keypair='$(CLUSTER_KEYPAIR)'" \
					 --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
					 --extra-vars="jump_host=' -F $(THIS_BASE)/ssh.config $(BIFROST_CLUSTER_NAME) '" \
					 --extra-vars="inventory_file=../../../../$(K8S_CLUSTER_INVENTORY)" $(V)

k8s_common:  ## Kubernetes apply the common roles
	[ ! -e $(K8S_VARS) ] || touch $(K8S_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(K8S_CLUSTER_INVENTORY) $(STACK_CLUSTER_PLAYBOOKS)/common.yml \
					  -e @$(THIS_BASE)/$(K8S_VARS) \
					  --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
					  --extra-vars="$(K8S_EXTRA_VARS)" $(V)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(K8S_CLUSTER_INVENTORY) $(DOCKER_PLAYBOOKS)/docker.yml \
					  -e @$(THIS_BASE)/$(K8S_VARS) \
					  --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)' activate_nvidia=$(NVIDIA) ignore_nvidia_fail=$(IGNORE_NVIDIA_FAIL) " \
					  --extra-vars="$(K8S_EXTRA_VARS)" $(V)

k8s_build_docker: ## Kubenetes apply docker role
	[ ! -e $(K8S_VARS) ] || touch $(K8S_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(K8S_CLUSTER_INVENTORY) $(DOCKER_PLAYBOOKS)/docker.yml \
					  -e @$(THIS_BASE)/$(K8S_VARS) \
					  --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)' activate_nvidia=$(NVIDIA) ignore_nvidia_fail=$(IGNORE_NVIDIA_FAIL) " \
					  --extra-vars="$(K8S_EXTRA_VARS)" \
					  --tags docker \
					  --limit $(K8S_NODES) \
					  $(V)

k8s_build_containerd: ## Kubenetes apply docker role
	[ ! -e $(K8S_VARS) ] || touch $(K8S_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(K8S_CLUSTER_INVENTORY) $(DOCKER_PLAYBOOKS)/docker.yml \
					  -e @$(THIS_BASE)/$(K8S_VARS) \
					  --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)' activate_nvidia=$(NVIDIA) ignore_nvidia_fail=$(IGNORE_NVIDIA_FAIL) " \
					  --extra-vars="$(K8S_EXTRA_VARS)" \
					  --tags containerd \
					  --limit $(K8S_NODES) \
					  $(V)

k8s_build: ## all the k8s node setup steps
	# make k8s_nodes  # cannot do k8s_nodes anymore because of VM image
	make k8s_common
	make build_haproxy
	make build_k8s

k8s_tell_next_worker:
	openstack server list --name="systems-k8s1-worker*" -f value -c Name | \
	python3 -c "import sys; m=[int(line.strip().split('-').pop()) for line in sys.stdin]; print('run: make k8s_add_worker WORKER=systems-k8s1-worker-'+str(sorted(m).pop()+1))"

k8s_add_worker: ## add a new worker - pass: WORKER=systems-k8s1-worker-N
	@if [ "" = "$(WORKER)" ]; then \
	echo "WORKER not set, use: 'WORKER=systems-k8s1-worker-N' - abort!"; exit 1; fi
	make k8s_nodes K8S_VARS=k8s_worker_vars.yml K8S_EXTRA_VARS='{"cluster_name": "$(WORKER)", "genericnode_worker": {"name": "$(WORKER)", "flavor": "le2.small", "image": "ubuntu-focal-20.04-nogui", "num_nodes": 1}}' K8S_CLUSTER_INVENTORY=inventory_k8s_$(WORKER)
	make k8s_common K8S_VARS=k8s_worker_vars.yml K8S_EXTRA_VARS= K8S_CLUSTER_INVENTORY=inventory_k8s_$(WORKER)
	# do the join !!!!!!
	mkdir -p k8s_combined_inventory
	rm -f k8s_combined_inventory/*
	cp $(K8S_CLUSTER_INVENTORY) k8s_combined_inventory/
	cp inventory_k8s_$(WORKER) k8s_combined_inventory/
	make k8s_distribute_kubeconfig
	make build_k8s K8S_VARS=k8s_worker_vars.yml K8S_CLUSTER_INVENTORY=k8s_combined_inventory \
	 TAGS="--limit $(WORKER),systems-k8s1-master-0,systems-k8s1-loadbalancer --tags k8s,join"

install_nvidia_drivers: ## install nvidia drivers and nvidia container runtime if gpu is present on machine
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -i ./$(K8S_CLUSTER_INVENTORY) ./playbooks/nvidia.yml \
		-e @$(THIS_BASE)/$(K8S_VARS) $(V)

install_nvidia_device_plugin:
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -i $(K8S_CLUSTER_INVENTORY) $(K8S_PLAYBOOKS)/nvidia_device_plugin.yml \
		-e @$(THIS_BASE)/$(K8S_VARS) $(V)

k8s_add_gpu_worker: ## add a new worker - pass: WORKER=systems-k8s1-<gpu>-worker-N
	@if [ "" = "$(WORKER)" ]; then \
	echo "WORKER not set, use: 'WORKER=systems-k8s1-<gpu>-worker-N' - abort!"; exit 1; fi
	@if [ "" = "$(FLAVOR)" ]; then \
	echo "FLAVOR not set, use: 'FLAVOR=<flavor>' - abort!"; exit 1; fi
	make k8s_nodes K8S_VARS=k8s_worker_vars.yml K8S_EXTRA_VARS='{"cluster_name": "$(WORKER)", "genericnode_worker": {"name": "$(WORKER)", "flavor": "$(FLAVOR)", "image": "ubuntu-focal-20.04-nogui", "num_nodes": 1}}' K8S_CLUSTER_INVENTORY=inventory_k8s_$(WORKER)
	make install_nvidia_drivers K8S_CLUSTER_INVENTORY=inventory_k8s_$(WORKER)
	make k8s_common K8S_VARS=k8s_worker_vars.yml K8S_EXTRA_VARS= K8S_CLUSTER_INVENTORY=inventory_k8s_$(WORKER)
	# do the join !!!!!!
	mkdir -p k8s_combined_inventory
	rm -f k8s_combined_inventory/*
	cp $(K8S_CLUSTER_INVENTORY) k8s_combined_inventory/
	cp inventory_k8s_$(WORKER) k8s_combined_inventory/
	make k8s_distribute_kubeconfig
	make build_k8s K8S_VARS=k8s_worker_vars.yml K8S_CLUSTER_INVENTORY=k8s_combined_inventory \
	 TAGS="--limit $(WORKER),systems-k8s1-master-0,systems-k8s1-loadbalancer --tags k8s,join"
	make install_nvidia_device_plugin K8S_CLUSTER_INVENTORY=k8s_combined_inventory

k8s_distribute_kubeconfig: ## k8s distribute kubeconfig to workers
	mkdir -p k8s_combined_inventory
	rm -f k8s_combined_inventory/*
	cp $(K8S_CLUSTER_INVENTORY) k8s_combined_inventory/
	cp inventory_k8s_* k8s_combined_inventory/ || true
	ansible-playbook -i ./k8s_combined_inventory elasticstack/playbooks/get_kubeconf.yml

integrate_k8s: # all the steps to integrate new k8s workers
	make dns
	make update_admins
	make k8s_distribute_kubeconfig
	make logging LOGGING_NODES=systems_k8s1_worker
	make node-exporter MONITORING_NODES=systems_k8s1_worker
	make integrate


k8s: build_k8s build_haproxy ## Build haproxy and k8s

build_haproxy: ## Build haproxy
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -i $(K8S_CLUSTER_INVENTORY) $(K8S_PLAYBOOKS)/loadbalancers.yml \
					  -e @$(THIS_BASE)/$(K8S_VARS) \
					  --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
					  --extra-vars="$(K8S_EXTRA_VARS)" $(V)

build_k8s: ## Build k8s
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -i $(K8S_CLUSTER_INVENTORY) --skip-tags containerd $(K8S_PLAYBOOKS)/setup.yml \
					  -e @$(THIS_BASE)/$(K8S_VARS) \
					  --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
					  --extra-vars="$(K8S_EXTRA_VARS)" \
	--extra-vars="activate_containerd=$(CONTAINERD) activate_nvidia=$(NVIDIA) ignore_nvidia_fail=$(IGNORE_NVIDIA_FAIL) debug=$(DEBUG)" \
	 $(TAGS) $(V)

build_charts: ## Build charts
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -i $(K8S_CLUSTER_INVENTORY) $(K8S_PLAYBOOKS)/charts.yml \
					  -e @$(THIS_BASE)/$(K8S_VARS) \
					  --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
					  --extra-vars="$(K8S_EXTRA_VARS)" \
	--extra-vars="activate_containerd=$(CONTAINERD) activate_nvidia=$(NVIDIA) ignore_nvidia_fail=$(IGNORE_NVIDIA_FAIL) debug=$(DEBUG)" $(V)

deploy_metallb: ## Build charts
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -i $(K8S_CLUSTER_INVENTORY) $(K8S_PLAYBOOKS)/metallb.yml \
					  -e @$(THIS_BASE)/$(K8S_VARS) \
					  --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
					  --extra-vars="$(K8S_EXTRA_VARS)" \
	--extra-vars="activate_containerd=$(CONTAINERD) activate_nvidia=$(NVIDIA) ignore_nvidia_fail=$(IGNORE_NVIDIA_FAIL) debug=$(DEBUG)" $(V)

deploy_binderhub: ## Deploy binderhub
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -i $(K8S_CLUSTER_INVENTORY) $(K8S_PLAYBOOKS)/binderhub.yaml \
					  -e @$(THIS_BASE)/$(K8S_VARS) \
					  --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
					  --extra-vars="$(K8S_EXTRA_VARS)" \
					  --extra-vars="azuread_client_id='$(AZUREAD_CLIENT_ID)' azuread_tenant_id='$(AZUREAD_TENANT_ID)' azuread_client_secret='$(AZUREAD_CLIENT_SECRET)' binderhub_oci_registry_password='$(BINDERHUB_OCI_REGISTRY_PASSWORD)'" \
					  --extra-vars="debug=$(DEBUG)" $(V)

rook: ## Install Rook Ceph support
	cd rook && make build_rook


nexus_node: ## Nexus Build nodes based on heat-cluster
	[ ! -e $(NEXUS_VARS) ] || touch $(NEXUS_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -i $(STACK_CLUSTER_PLAYBOOKS)/inventory \
					 $(STACK_CLUSTER_PLAYBOOKS)/cluster-infra.yml \
					 -e @$(THIS_BASE)/$(NEXUS_VARS) \
					 --extra-vars="$(NEXUS_EXTRA_VARS)" \
					 --extra-vars="cluster_keypair='$(CLUSTER_KEYPAIR)'" \
					 --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
					 --extra-vars="jump_host=' -F $(THIS_BASE)/ssh.config $(BIFROST_CLUSTER_NAME) '" \
					 --extra-vars="inventory_file=../../../../$(NEXUS_CLUSTER_INVENTORY)" $(V)

nexus_common:  ## Nexus apply the common roles
	[ ! -e $(NEXUS_VARS) ] || touch $(NEXUS_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(NEXUS_CLUSTER_INVENTORY) $(STACK_CLUSTER_PLAYBOOKS)/common.yml \
					  -e @$(THIS_BASE)/$(NEXUS_VARS) \
					  --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
					  --extra-vars="$(NEXUS_EXTRA_VARS)" $(V)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(NEXUS_CLUSTER_INVENTORY) $(DOCKER_PLAYBOOKS)/docker.yml \
					  -e @$(THIS_BASE)/$(NEXUS_VARS) \
					  --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
					  --extra-vars="$(NEXUS_EXTRA_VARS)" $(V)

nexus: ## call the Makefile in the nexus sub-project
	cp PrivateRules.mak -f nexus/PrivateRules.mak
	cp nexus_vars.yml -f nexus/nexus_vars.yml
	cp inventory_nexus -f nexus/inventory_nexus
	export ANSIBLE_SSH_ARGS="-o ForwardAgent=yes -F $(THIS_BASE)/ssh.config " && \
	cd nexus && \
	make install && \
	make nexus

# image_cache: nexus_node nexus_common nexus ## build all of Nexus # cannot do nexus_node anymore because of VM image

image_cache: nexus_common nexus ## build all of Nexus

mariadb_node: ## MariaDB Build nodes based on heat-cluster
	[ ! -e $(MARIADB_VARS) ] || touch $(MARIADB_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -i $(STACK_CLUSTER_PLAYBOOKS)/inventory \
					 $(STACK_CLUSTER_PLAYBOOKS)/cluster-infra.yml \
					 -e @$(THIS_BASE)/$(MARIADB_VARS) \
					 --extra-vars="$(MARIADB_EXTRA_VARS)" \
					 --extra-vars="cluster_keypair='$(CLUSTER_KEYPAIR)'" \
					 --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
					 --extra-vars="jump_host=' -F $(THIS_BASE)/ssh.config $(BIFROST_CLUSTER_NAME) '" \
					 --extra-vars="inventory_file=../../../../$(MARIADB_CLUSTER_INVENTORY)" $(V)

mariadb_common:  ## MariaDB apply the common roles
	[ ! -e $(MARIADB_VARS) ] || touch $(MARIADB_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(MARIADB_CLUSTER_INVENTORY) $(STACK_CLUSTER_PLAYBOOKS)/common.yml \
					  -e @$(THIS_BASE)/$(MARIADB_VARS) \
					  --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
					  --extra-vars="$(MARIADB_EXTRA_VARS)" $(V)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(MARIADB_CLUSTER_INVENTORY) $(DOCKER_PLAYBOOKS)/docker.yml \
					  -e @$(THIS_BASE)/$(MARIADB_VARS) \
					  --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
					  --extra-vars="$(MARIADB_EXTRA_VARS)" $(V)

mariadb: ## call the Makefile in the mariadb sub-project
	export ANSIBLE_SSH_ARGS="-o ForwardAgent=yes -F $(THIS_BASE)/ssh.config " && \
	cd mariadb && \
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -i $(THIS_BASE)/$(MARIADB_CLUSTER_INVENTORY) playbooks/setup.yml \
	-e @$(THIS_BASE)/$(MARIADB_VARS) \
	--extra-vars="mysql_root_password=$(MYSQL_ROOT_PASSWORD) mysql_database=$(MYSQL_DATABASE) mysql_user=$(MYSQL_USER) mysql_password=$(MYSQL_PASSWORD) mysql_privileges=$(MYSQL_PRIVILEGES)" \
	$(V)

# edadb: mariadb_node mariadb_common mariadb ## build all of MariaDB # cannot do mariadb_node anymore because of VM image

edadb: mariadb_common mariadb ## build all of MariaDB

influxdb_node: ## InfluxDB Build nodes based on heat-cluster
	[ ! -e $(INFLUXDB_VARS) ] || touch $(INFLUXDB_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -i $(STACK_CLUSTER_PLAYBOOKS)/inventory \
					 $(STACK_CLUSTER_PLAYBOOKS)/cluster-infra.yml \
					 -e @$(THIS_BASE)/$(INFLUXDB_VARS) \
					 --extra-vars="$(INFLUXDB_EXTRA_VARS)" \
					 --extra-vars="cluster_keypair='$(CLUSTER_KEYPAIR)'" \
					 --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
					 --extra-vars="jump_host=' -F $(THIS_BASE)/ssh.config $(BIFROST_CLUSTER_NAME) '" \
					 --extra-vars="inventory_file=../../../../$(INFLUXDB_CLUSTER_INVENTORY)" $(V)

influxdb_common:  ## InfluxDB apply the common roles
	[ ! -e $(INFLUXDB_VARS) ] || touch $(INFLUXDB_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(INFLUXDB_CLUSTER_INVENTORY) $(STACK_CLUSTER_PLAYBOOKS)/common.yml \
					  -e @$(THIS_BASE)/$(INFLUXDB_VARS) \
					  --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
					  --extra-vars="$(INFLUXDB_EXTRA_VARS)" $(V)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(INFLUXDB_CLUSTER_INVENTORY) $(DOCKER_PLAYBOOKS)/docker.yml \
					  -e @$(THIS_BASE)/$(INFLUXDB_VARS) \
					  --extra-vars="registry_mirror='$(REGISTRY_MIRROR)' docker_hub_mirror='$(REGISTRY_MIRROR)' podman_registry_mirror='$(PODMAN_REGISTRY_MIRROR)'" \
					  --extra-vars="$(INFLUXDB_EXTRA_VARS)" $(V)

influxdb: ## call the Makefile in the metricsdb sub-project
	export ANSIBLE_SSH_ARGS="-o ForwardAgent=yes -F $(THIS_BASE)/ssh.config " && \
	cd metricsdb && \
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -i $(THIS_BASE)/$(INFLUXDB_CLUSTER_INVENTORY) playbooks/setup.yml \
	-e @$(THIS_BASE)/$(INFLUXDB_VARS) \
	--extra-vars="mysql_root_password=$(MYSQL_ROOT_PASSWORD) mysql_database=$(MYSQL_DATABASE) mysql_user=$(MYSQL_USER) mysql_password=$(MYSQL_PASSWORD) mysql_privileges=$(MYSQL_PRIVILEGES)" \
	$(V)

# metricsdb: influxdb_node influxdb_common influxdb ## build all of InfluxDB # cannot do influxdb_node anymore because of VM image

metricsdb: influxdb_common influxdb ## build all of InfluxDB

help:  ## show this help.
	@echo "make targets:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo ""; echo "make vars (+defaults):"
	@grep -E '^[0-9a-zA-Z_-]+ \?=.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = " \\?= "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
production: false
